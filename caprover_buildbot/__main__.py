from sys import argv

if len(argv) < 2:
    from . import app
    app.run("0.0.0.0")
elif argv[1] == "build_all":
    from .builder import build
    build(True)
elif argv[1] == "build":
    from .builder import build
    build()
else:
    print("Invalid command.")
    print("Valid commands: build, build_all")
FROM python:3.7

WORKDIR /opt/app

RUN pip install poetry

ADD pyproject.toml .
ADD poetry.lock .

RUN poetry install --no-dev

ADD . .

EXPOSE 5000

CMD poetry run python -m caprover_buildbot
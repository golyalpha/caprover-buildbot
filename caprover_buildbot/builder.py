from tempfile import mkdtemp
from subprocess import Popen, PIPE
from os import path, walk
from json import loads
from shutil import rmtree

import git
from docker import DockerClient


docker = DockerClient.from_env()

images = {
    "caprover": "https://github.com/caprover/caprover.git",
    "netdata": "https://github.com/titpetric/netdata.git",
    "caprover-placeholder-app": "https://github.com/caprover/caprover-placeholder-app.git",
    "certbot-sleeping": "https://gitlab.com/golyalpha/certbot-sleeping.git"
}

gtags = {
    "caprover": True
}

dockerfile = {
    "caprover": "dockerfile-captain.release"
}

context = {
    "netdata": "releases/v1.8.0"
}

tags = {
    "caprover": "golyalpha/caprover:1.5.2",
    "netdata": "golyalpha/netdata:1.8",
    "caprover-placeholder-app": "golyalpha/caprover-placeholder-app",
    "certbot-sleeping": "golyalpha/certbot-sleeping:v0.29.1"
}

def build(all=False):
    patchdir = _grab_patches()
    if not all:
        d = _grab_repositories("caprover")
        _apply_patch(d, patchdir, "caprover")
        _build_container(d, "caprover")
        _publish_container("caprover")
        _cleanup_dir(d)
        _cleanup_dir(patchdir)
        return
    for project in images:
        d = _grab_repositories(project)
        _apply_patch(d, patchdir, project)
        _build_container(d, project)
        _publish_container(project)
        _cleanup_dir(d)
    _cleanup_dir(patchdir)


def _grab_patches():
    d = mkdtemp()
    print("Cloning patches into", d)
    git.Repo.clone_from("https://github.com/golyalpha/caprover-patches.git", d)
    return d



def _grab_repositories(project):
    d = mkdtemp()
    print("Cloning", project, "into", d)
    r = git.Repo.clone_from(images[project], d)
    g = git.Git(d)
    g.init()
    try:
        print("Checking out tag", r.tags[-1].name, "on", project)
        g.checkout(r.tags[-1].name)
    except IndexError:
        print("No tag to check out.")
    return d



def _apply_patch(d, patchdir, project):
    if path.exists(path.join(patchdir, project)):
        print("Applying patch for", project)
        for root, dirs, files in walk(path.join(patchdir, project)):
            for f in files:
                with open(path.join(root, f), "rb") as patch:
                    with Popen(["patch", "-p1", "-b"], stdin=PIPE, stdout=PIPE, stderr=PIPE, cwd=d) as patcher:
                        stdout, stderr = patcher.communicate(patch.read())
                        print(stdout)
                        print(stderr)


def _build_container(d, project):
    print("Building container for", project)
    img, output = docker.images.build(
        path=path.join(d, context.get(project, "")),
        rm=True,
        tag=tags[project],
        dockerfile=dockerfile.get(project, "Dockerfile")
    )
    for obj in output:
        try:
            print(obj["stream"].strip())
        except KeyError:
            print(obj)


def _publish_container(project):
    print("Publishing container for", project)
    for line in docker.images.push(tags[project], stream=True):
        data = loads(line.strip())
        try:
            print(data["status"], data.get("id", ""), data.get("progress", ""))
        except KeyError:
            print(data.get("error", data))

def _cleanup_dir(d):
    print("Cleaning up", d)
    rmtree(d)
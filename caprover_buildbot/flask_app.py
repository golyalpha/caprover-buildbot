from flask import Flask, Response
from subprocess import Popen, PIPE

from .builder import build

app = Flask(__name__)

@app.route("/build", methods=["GET", "POST"])
def _build():
    def _start_build():
        with Popen(
            ["poetry", "run", "python", "-m", "caprover_buildbot", "build"],
            stdout=PIPE,
            stderr=PIPE,
            bufsize=1, universal_newlines=True
        ) as builder:
            yield "STDOUT:\n"
            for line in builder.stdout:
                yield line
            yield "\n"
            yield "\n"
            yield "STDERR:\n"
            for line in builder.stderr:
                yield line
    return Response(_start_build(), mimetype="text/plain")


@app.route("/build_all", methods=["GET", "POST"])
def _build_all():
    def _start_build():
        with Popen(
            ["poetry", "run", "python", "-m", "caprover_buildbot", "build_all"],
            stdout=PIPE,
            stderr=PIPE,
            bufsize=1, universal_newlines=True
        ) as builder:
            yield "STDOUT:\n"
            for line in builder.stdout:
                yield line
            yield "\n"
            yield "\n"
            yield "STDERR:\n"
            for line in builder.stderr:
                yield line

    return Response(_start_build(), mimetype="text/plain")